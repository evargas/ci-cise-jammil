
/* global response */

$('#btn-create').on('click',() => {
   
    var url_action = $('.frm-create').attr('action');
    var datosForm = $('.frm-create').serialize();
    $('#btn-create').text('Enviando...').attr("disabled", true)
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: url_action,
        data: datosForm,
        async: true,
        dataType: 'json',
        success: response =>  {
            // if (response.success) {
            //     $('.frm-create')[0].reset();
            //     location.reload();
            //      window.alert('si');
            // } else {
            //     alert('error');
            // }
            setTimeout(() => {
                $('#btn-create').text('Guardado -> '+ response.id).attr("disabled", false)
            }, 1000)
        },
        error: e => {
            console.error({error: e});
        }
    });
});


$('#preciocompra').keypress(function (event) {
    var $this = $(this);
    if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
            ((event.which < 48 || event.which > 57) &&
                    (event.which != 0 && event.which != 8))) {
        event.preventDefault();
    }

    var text = $(this).val();
    if ((event.which == 46) && (text.indexOf('.') == -1)) {
        setTimeout(function () {
            if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
            }
        }, 1);
    }

    if ((text.indexOf('.') != -1) &&
            (text.substring(text.indexOf('.')).length > 2) &&
            (event.which != 0 && event.which != 8) &&
            ($(this)[0].selectionStart >= text.length - 2)) {
        event.preventDefault();
    }
});


$('#precioventa').keypress(function (event) {
    var $this = $(this);
    if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
            ((event.which < 48 || event.which > 57) &&
                    (event.which != 0 && event.which != 8))) {
        event.preventDefault();
    }

    var text = $(this).val();
    if ((event.which == 46) && (text.indexOf('.') == -1)) {
        setTimeout(function () {
            if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
            }
        }, 1);
    }

    if ((text.indexOf('.') != -1) &&
            (text.substring(text.indexOf('.')).length > 2) &&
            (event.which != 0 && event.which != 8) &&
            ($(this)[0].selectionStart >= text.length - 2)) {
        event.preventDefault();
    }
});


//Letras - NUMEROS - TILDES - espacio y -
$('#producto').on('input', function (e) {
    if (!/^[ a-z0-9áéíóúüñ-]*$/i.test(this.value)) {
        this.value = this.value.replace(/[^ a-z0-9áéíóúüñ]+/ig, "");
    }
});