<footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019 <b>Version</b> <?php echo APP_VERSION; ?> </span>
          </div>
        </div>
      </footer>