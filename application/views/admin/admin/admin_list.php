<!--------------------------------------INICIO------------------------------------------------------>

<div class="container-fluid">
    <!-- Page Heading -->

    <div class="card shadow mb-4">

        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Listado de Productos</h6>
        </div>
        <div class="card-body">


            <div class="col-sm-4 col-md-3 col-lg-3 mb-4">
                <a class="btn btn-success btn-block" href="<?php echo base_url() ?>admin/admin_saveproducto">Agregar Producto <i class="fas fa-plus-circle" style="padding-left:20px;"></i></a>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Codigo</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Categoria</th>
                            <th scope="col">Precio Compra</th>
                            <th scope="col">Precio Venta</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Opciones</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Codigo</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Categoria</th>
                            <th scope="col">Precio Compra</th>
                            <th scope="col">Precio Venta</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Opciones</th>
                        </tr>
                    </tfoot>
                    <tbody id="myTable">
                        <?php if (!empty($listarProductos)): ?>
                            <?php foreach ($listarProductos as $key => $p) : ?>
                                <tr>
                                    <td><?php
                                        $n = 1;
                                        echo $n = $n + $key;
                                        ?></td>
                                    <td> <?php echo $p->codigoPro ?> </td>
                                    <td> <?php echo word_limiter($p->producto, 4); ?> </td>
                                    <td> <?php echo word_limiter($p->categoria, 2); ?> </td>
                                    <td> <?php echo $p->precioCom ?> </td>
                                    <td> <?php echo $p->preciVen ?> </td>
                                    <!---  <td> <? php/* echo $p->img1 !=""? '<img class="img-thumbnail img-presentation-small" src="'.base_url().'uploatods/producs/'.$p->img1 .'">':"" */ ?> </td>-->
                                    <td> <?php echo format_date($p->fecha) ?> </td>
                                    <td align="center">
                                        <button class="btn btn-success btn-sm btn-view" data-toggle="modal" value="<?php echo $p->idProduct ?>" data-target="#verModal"><i class="fas fa-search fa-sm"></i></button>

                                        <a class="btn btn-warning btn-sm" href="<?php echo base_url() . 'admin/admin_saveproducto/' . $p->idProduct ?>"><i class="far fa-edit"></i></a>

                                        <a class="btn btn-danger btn-sm btn-view" href="" data-toggle="modal" data-target="#deleteModal" data-product_id="<?php echo $p->idProduct ?>">

                                            <i class="fas fa-trash-alt"></i></a>
                                    </td>
                                </tr>

                            <?php endforeach; ?>
                        <?php endif; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- End of Main Content -->
</div>
<!--------------------------------------INICIO------------------------------------------------------>

<!----------------------------MODAL Delete----------------------------------------->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <h5>Seguro que desea eliminar el producto seleccionado </h5>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secundary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger" id="borrar-producto" data-dismiss="modal">Eliminar</button>
            </div>
        </div>
    </div>
</div>
<!----------------------------MODAL Delete FIN----------------------------------------->

<!----------------------------MODAL VER INICIO----------------------------------------->
<!-- Button trigger modal -->


<!-- Modal -->

<!-- The Modal -->


<div class="modal fade bd-example-modal-xl" tabindex="-1"id="verModal" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" style="color:#141216">Detalle Del Producto</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                
                
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<!----------------------------MODAL VER FIN-------------------------------------------->
<script>
    var product_id = 0;
    var buttondelete;
//abrir el modal

    $('#deleteModal').on('show.bs.modal', function (event) {
        buttondelete = $(event.relatedTarget) // Button that triggered the modal
        product_id = buttondelete.data('product_id') // Traemos el id
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('Eliminar Producto')

    });

//desarrollar conexion ajax para llamar a eliminar

    $("#borrar-producto").click(function () {

        $.ajax({
            url: "<?php echo base_url() ?>admin/product_delete/" + product_id
        }).done(function (res) {
            if (res == 1) {
                $(buttondelete).parent().parent().remove();
            }

        });

    });


    $(document).ready(function () {

        var base_url = "<?php echo base_url(); ?>";

        $(".btn-view").on("click", function () {


            var id = $(this).val();
            $.ajax({

                url: base_url + "admin/product_view/" + id,
                type: "POST",
                dataType: "html",
                data: {id: id},
            }).done(function (data) {
                $("#verModal .modal-body").html(data);
            });

        });

        /*Tabla en español*/
        $('#dataTable').DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por pagina",
                "zeroRecords": "No se encontraron resultados en su busqueda",
                "searchPlaceholder": "Buscar registros",
                "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
                "infoEmpty": "No existen registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "search": "Buscar:",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
            }
        });

    })
</script>

