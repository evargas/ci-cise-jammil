<!--------------------------------------INICIO------------------------------------------------------>





<div class="card shadow mb-12">

    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary"><?php echo $title; ?></h6>
    </div>

    <?php echo form_open('', 'class="card-body frm-create" enctype="multipart/form-data"  autocomplete="off"'); ?>

    <div class="row">

        <div class="col-md-4">
            <div class="form-group row">
                <label class="col-sm-6 col-form-label">Producto:</label>
                <div class="col-sm-8">
                    <?php
                    $text_input = array(

                        'name'     => 'producto',
                        'type'     => 'text',
                        'id'       => 'producto',
                        'value'    => $producto,
                        'class'    => 'form-control',
                        'maxlength'=>  "100",
                      //  'onkeypress' =>"return soloLetrasNumeros(event)",
                    );

                    echo form_input($text_input);
                    ?>
                    <?php echo form_error('producto', '<div class="alert alert-danger" role="alert">
                                        "', '"
                                         </div>');  ?>

                </div>
            </div>


            <div class="form-group row">
                <label class="col-sm-6 col-form-label">Estado:</label>
                <div class="col-sm-8">
                    <?php
                    echo form_dropdown('estado', $data_estado, $estado, 'class="form-control input-lg"');
                    ?>
                </div>
            </div>
        </div>

        <div class="col-md-4">

            <div class="form-group row">
                <label class="col-sm-6 col-form-label">Codigo:</label>
                <div class="col-sm-8">
                    <?php
                    $text_input = array(

                        'name'     => 'codigo',
                        'type'     => 'text',
                        'id'       => 'codigo',
                        'value'    => $codigoPro,
                        'class'    => 'form-control',
                    );

                    echo form_input($text_input);
                    ?>
                    <?php echo form_error('codigo', '<div class="alert alert-danger" role="alert">
                                        "', '"
                                         </div>');  ?>
                </div>
            </div>


            <div class="form-group row">
                <label class="col-sm-8 col-form-label">Categoria:</label>
                <div class="col-sm-8">
                    <?php
                    echo form_dropdown('categoria', $data_categoria, $categoria, 'class="form-control input-lg"');
                    ?>
                </div>
            </div>

        </div>

        <div class="col-md-4">


            <div class="form-group row">
                <label class="col-sm-6 col-form-label">Cambio:</label>
                <div class="col-sm-8">

                    <?php
                    $text_input = array(

                        'name'     => 'tipocambio',
                        'type'     => 'text',
                        'id'       => 'tipocambio',
                        'value'    => $tipoCamb,
                        'class'    => 'form-control',
                    );

                    echo form_input($text_input);
                    ?>

                    <?php echo form_error('tipocambio', '<div class="alert alert-danger" role="alert">
                                        "', '"
                                         </div>');  ?>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-6 col-form-label">Moneda:</label>
                <div class="col-sm-8">

                    <?php
                    echo form_dropdown('moneda', $data_moneda, $moneda, 'class="form-control input-lg"');
                    ?>

                </div>
            </div>

        </div>

    </div>

    <div class="row" style="border-top: 1px solid #d6d9dc; padding-top: 13px; ">

        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-6 col-form-label">Precio Compra:</label>
                <div class="col-sm-8">
                    <?php
                    $text_input = array(

                        'name'     => 'preciocompra',
                        'type'     => 'text',
                        'id'       => 'preciocompra',
                        'value'    => $precioCom,
                        'class'    => 'form-control',
                    );

                    echo form_input($text_input);
                    ?>
                    <?php echo form_error('preciocompra', '<div class="alert alert-danger" role="alert">
                                        "', '"
                                         </div>');  ?>

                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-6 col-form-label">Precio Venta:</label>
                <div class="col-sm-8">
                    <?php
                    $text_input = array(

                        'name'     => 'precioventa',
                        'type'     => 'text',
                        'id'       => 'precioventa',
                        'value'    => $preciVen,
                        'class'    => 'form-control',
                    );

                    echo form_input($text_input);
                    ?>
                    <?php echo form_error('precioventa', '<div class="alert alert-danger" role="alert">
                                        "', '"
                                         </div>');  ?>

                </div>
            </div>
        </div>

    </div>


    <div class="row" style="border-top: 1px solid #d6d9dc; padding-top: 13px; ">

        <div class="col-md-12">
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Descripcion</label>

                <?php $text_area = array(
                    'name'      => 'descripcion',
                    'id'        => 'descripcion',
                    'value'     => $descripcion,
                    'class'     => 'form-control',
                    'rows'       => '4'
                );

                echo form_textarea($text_area);
                ?>
                <?php echo form_error('descripcion', '<div class="alert alert-danger" role="alert">
                                        "', '"
                                         </div>');  ?>
            </div>
        </div>

    </div>

    <!------------------------------------------------------------IMAGENES INICIO------------------------------------------------------------------>
    <div class="form-group">

                <?php echo form_label('Imagen', 'image'); ?>

                <?php $text_input = array(
                    'name' => 'image',
                    'id'   => 'image',
                    'value' => '',
                    'type' => 'file',
                    'class' => 'form-control input-lg'
                );

                echo form_input($text_input);

                ?>


            </div>

    
    <div class="row" style="border-top: 1px solid #d6d9dc; padding-top: 13px; ">

        <div class="col-sm">
            
        </div>


        <div class="col-sm">

        </div>


        <div class="col-sm">


        </div>


        <div class="col-sm">


        </div>

        <div class="col-sm">

        </div>

    </div>

    <!--------------------------------------------------IMAGENES FINAL-------------------------------------------------------------------------->
    <div class="container-fluid">

        <div class="row">

            <div class="col">
                <?php /*echo form_submit('mysubmit', $title , 'class="btn btn-success btn-block"')*/ ?>
                <button type="button" class="btn btn-success btn-block" id="btn-create">Create</button>
            </div>

            <div class="col">
                <a class="btn btn-info btn-block" href="<?php echo base_url() . 'admin/admin_listProduc/' ?>">Cancelar</a>
            </div>

        </div>
    </div>

    <?php echo form_close() ?>
</div>



<!--------------------------------------FIN------------------------------------------------------>



<!------------------------------------------------------------------------------------>
<!------------------------------------------------------------------------------------>
<script>


//JQuery permite solo dos números después del punto decimal

</script>