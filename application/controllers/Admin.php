<?php

class Admin extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->database();
        //Cargamos libreria - empleamos los template - reducimos codigo
        //Codigo mjor organizado

        $this->load->library("parser");
        $this->load->library("Form_validation");

        $this->load->helper("url");
        $this->load->helper('form');
        $this->load->helper('text');

        //Helpers
        $this->load->helper('Produc_helper');
        $this->load->helper('Date_helper');

        $this->load->model('Product');
    }

    public function index() {

        $this->load->view("admin/test");
    }

    public function admin_listProduc() {

        $data["listarProductos"] = $this->Product->findAll();
        $view["body"] = $this->load->view("admin/admin/admin_list", $data, TRUE);
        $this->parser->parse("admin/template/body", $view);
    }

    public function admin_saveproducto($product_id = null) {

        // if ($product_id == null) {
        //     //Creamos el producto
                $data['producto'] = $data['codigoPro'] = $data['estado'] = $data['categoria']  
               = $data['moneda']  = $data['tipoCamb'] =  $data['precioCom'] =  $data['preciVen']
               = $data['descripcion'] = "";
        //      //Acccion
              $data['title'] = "Registrar Producto";
              $data['boton_title'] = "Registrar";
        // } else {
        //     // editamos el producto
        //     $product = $this->Product->find($product_id);
        //     $data['producto']       = $product->producto;
        //     $data['codigoPro']      = $product-> codigoPro;
        //     $data['estado']         = $product->estado;
        //     $data['categoria']      = $product->categoria;
        //     $data['moneda']         = $product->moneda;
        //     $data['tipoCamb']       = $product->tipoCamb;
        //     $data['precioCom']      = $product->precioCom;
        //     $data['preciVen']       = $product->preciVen;
        //     $data['descripcion']    = $product->descripcion;
        //     $data['title'] = "Actualizar Producto";
        //     $data['boton_title'] = "Actualizar";
        // }

        // if ($this->input->server('REQUEST_METHOD') == "POST") {
        if ($this->input->post()) {

            //El siguiente codigo hace referencia a la importacion validation
            //Orden :: nombre del campo :: nombre con el que se muestra el error ::Reglas de validaciones
            // $this->form_validation->set_rules('producto', 'Producto', 'required|min_length[4]|max_length[100]');
            // $this->form_validation->set_rules('codigo', 'Codigo', 'required|min_length[2]|max_length[20]');
            // $this->form_validation->set_rules('estado', 'Estado', 'required');
            // $this->form_validation->set_rules('categoria', 'Categoria', 'required');
            // $this->form_validation->set_rules('moneda', 'Moneda', 'required');
            // $this->form_validation->set_rules('tipocambio', 'T.Cambio', 'required');
            // $this->form_validation->set_rules('preciocompra', 'P.Compra', 'required');
            // $this->form_validation->set_rules('precioventa', 'P.Venta', 'required');
            // $this->form_validation->set_rules('descripcion', 'Descripcion', 'max_length[500]');
                
            
            // $data['producto']       = $this->input->post('producto');
            // $data['codigoPro']      = $this->input->post('codigo');
            // $data['estado']         = $this->input->post('estado');
            // $data['categoria']      = $this->input->post('categoria');
            // $data['moneda']         = $this->input->post('moneda');
            // $data['tipoCamb']       = $this->input->post('tipocambio');
            // $data['precioCom']      = $this->input->post('preciocompra');
            // $data['preciVen']       = $this->input->post('precioventa');
            // $data['descripcion']    = $this->input->post('descripcion'); 
         
            //salvar nuestro post, pero antes verificamos que nuestros datos cumplen con todas las reglas

            // if ($this->form_validation->run()) {
                //Nuestro form es valido
                $save = array(
                    'producto' => $this->input->post('producto'),
                    'codigoPro' => $this->input->post('codigo'),
                    'estado' => $this->input->post('estado'),
                    'categoria' => $this->input->post('categoria'),
                    // 'moneda' => $this->input->post('moneda'),
                    'tipoCamb' => $this->input->post('tipocambio'),
                    'precioCom' => $this->input->post('preciocompra'),
                    'preciVen' => $this->input->post('precioventa'),
                    'descripcion' => $this->input->post('descripcion'),
                );
                //guardar la data

                 // if($product_id == null){
                     
                 $product_id = $this->Product->insert($save);
                // redirect(base_url()."admin/admin_listProduc");
                
                 // }
                 // else{
                     // $this->Product->update($product_id,$save);
                 // }
                //Cargar Imagen
                $this->upload($product_id, $this->input->post('producto'));
            // }else{
                $msg['id'] = $product_id;		
		          echo json_encode($msg);
                  exit;
                //echo json_encode('Error al guardar');
            // }
        }else{
            // Llamamos a nuestra funcion(helper) que se encuentra en (helper/Product_helper)
            $data["data_estado"] = estado();
            $data["data_categoria"] = categoria();
            $data["data_moneda"] = moneda();
            $view["body"] = $this->load->view("admin/admin/saveproduct", $data, TRUE);
            // $view["body"] hace referencia a la variable contenida en el template body
            //esta vista es de contenido de la variable body(admin/template/body) definido en el template siguiente:

            $this->parser->parse("admin/template/body", $view);
        }
        
    }

    //$product_id = null significa que si el id existe, sino  este sera igual a null
    public function product_delete($product_id = null) {

        if ($product_id == null) {
            echo 0;
        } else {
            $this->Product->delete($product_id);
            echo 1;
        }
    }
    //vista detalle
    public function product_view($product_id = null){
        if ($product_id == null) {
            echo 'Error';
        } else {
           
            $data = array(
                'producto' =>$this->Product->find($product_id),
            );
             
             $this->load->view("admin/detalle/viewProduc", $data);
            
        } 
    }
    //metodo para cargar imagenes
    private function upload($product_id, $product) {

        $image = "image"; //Nombre del input image
        //Nuevo metodo
        $product = clean_name($product);

        //Configuracion de carga
        $config['upload_path'] = 'uploads/productos'; //ruta
        $config['file_name'] = $product; //nombre del archivo
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 5000;
        $config['overwrite'] = TRUE;
        /* $config['max_width']         = 1024;
          $config['max_height']        = 768; */
        //cargamos la libreria
        $this->load->library('upload', $config);

        if ($this->upload->do_upload($image)) {
            //se cargo la imagen
            //Datos del upload
            $data = $this->upload->data();

            $save = array(
                'img1' => $product . $data["file_ext"]
            );
            //llamamos a la funcion upload(system/model/) y cargamos la imagen
            $this->Product->update($product_id, $save);
            //Redimencinar imagenen
            $this->resize_image($data['full_path'], $product . $data["file_ext"]);
        }
    }

    //Funcion para Redimencionar Imagen
    function resize_image($path_image) {

        $config['image_library'] = 'gd2';
        $config['source_image'] = $path_image;
        $config['maintain_ratio'] = TRUE;
        // $config['create_thumb']     = FALSE;
        $config['width'] = 500;
        $config['height'] = 500;

        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
    }

}
